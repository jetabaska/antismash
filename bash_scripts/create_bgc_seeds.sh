#!/bin/bash

PREFIX=${1}/antismash/generic_modules/hmm_detection

pushd ${PREFIX}

grep ".hmm" hmmdetails.txt | awk -F $'\t' '{print $4}' | xargs cat > bgc_seeds.hmm

popd
